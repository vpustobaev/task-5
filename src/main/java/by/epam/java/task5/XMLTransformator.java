package by.epam.java.task5;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XMLTransformator {

    public static void main(String[] args) {

	try {

	    TransformerFactory tf = TransformerFactory.newInstance();

	    Transformer transformer = tf.newTransformer(new StreamSource("src/main/resources/gems.xsl"));

	    transformer.transform(new StreamSource("src/main/resources/gems.xml"),
		    new StreamResult("src/main/resources/diamandFund.html"));

	    System.out.println("Transform complete");

	} catch (TransformerException ex) {
	    System.err.println("Impossible to transform file " + ex);

	}

    }

}
